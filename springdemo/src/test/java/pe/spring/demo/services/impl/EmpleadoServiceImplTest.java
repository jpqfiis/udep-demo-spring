package pe.spring.demo.services.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import pe.spring.demo.dao.SeguroDao;
import pe.spring.demo.entities.Empleado;
import pe.spring.demo.repo.IEmpleadoRepo;
import pe.spring.demo.services.impl.EmpleadoServiceImpl;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/appconfig-root.xml"})
@WebAppConfiguration
public class EmpleadoServiceImplTest {
	
	@InjectMocks
	private EmpleadoServiceImpl empleadoServiceImpl;
	
	@Mock
	private IEmpleadoRepo repo;
	@Mock
	private SeguroDao seguroDao;
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void guardarEmpleado_ObtieneSiempreId() {
		
		Empleado empleadoRepo = new Empleado();
		empleadoRepo.setId(1);
		
		when(repo.save(any())).thenReturn(empleadoRepo);
		
		Empleado obj = empleadoServiceImpl.guardarEmpleado(new Empleado());
		
		assertNotNull(obj.getId());
	}

}
