package pe.spring.demo.repo;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import pe.spring.demo.entities.Empleado;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/appconfig-root.xml"})
@WebAppConfiguration
@Transactional
@Rollback
public class EmpleadoRepoTest {
	
	@Autowired
	private IEmpleadoRepo empleado;
	
	@Test
	public void obtenerListadoEmpleados_EsNoNulo() {
		
		List<Empleado> listado = empleado.findAll();
		
		assertTrue(listado.size() > 0);
	}
	
	@Test
	public void meotodoSaveRepo_ObtieneEmpleadoConId() {
		Empleado empl = new Empleado();
		empl.setNombre("Luis");
		empl.setDepartamento("Ventas");
		empl.setTelefono("98786768");
		empl.setSexo("masculino");
		
		Empleado obj = empleado.save(empl);
		
		assertNotNull(obj.getId());
	}
	

}
