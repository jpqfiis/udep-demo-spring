package pe.spring.demo.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import pe.spring.demo.entities.Empleado;
import pe.spring.demo.entities.TablaMaestra;
import pe.spring.demo.services.IEmpleadoService;
import pe.spring.demo.services.ITablaMaestraService;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/appconfig-root.xml"})
@WebAppConfiguration
public class EmpleadoControllerTest {
	
	@InjectMocks
	private EmpleadoController empleadoController;
	
	@Mock
	private Model model;
	@Mock
	private IEmpleadoService empleadoService;
	@Mock
	private ITablaMaestraService tablaMaestraService;
	@Mock
	private BindingResult binding;
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Ignore
	public void validarObtenerViewEmpleado() throws Exception {
		
		when(empleadoService.listarPorId(1)).thenReturn(new Empleado());
		when(tablaMaestraService.listar()).thenReturn(new ArrayList<TablaMaestra>());
		
		String view = empleadoController.getView(1, model);
		
		assertEquals("empleado", view);
	}
	
	@Test
	public void validarProcessForm_BindingResultFalse() {
		when(binding.hasErrors()).thenReturn(false);
		String view = empleadoController.processForm(new Empleado(), binding, model);
		
		assertEquals("redirect:/planilla", view);
	}
	
	@Test
	public void validarProcessForm_BindingResultTrue() {
		when(binding.hasErrors()).thenReturn(true);
		String view = empleadoController.processForm(new Empleado(), binding, model);
		
		assertEquals("empleado", view);
	}

}
