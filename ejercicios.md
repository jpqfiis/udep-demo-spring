# Ejercicio 1

Se requiere que los **usuarios administradores** del sistema tengan la opci�n de cambiar la contrase�a que se les asign� por defecto. Para ello se basar�n en un correo al que llegar� una notificaci�n con un **link temporal de dos horas** . Al dar clic en el link se mostrar� un formulario de cambio de contrase�a donde se pedir�:
* Contrase�a antigua
* Contrase�a nueva.
* Confirmar contrase�a nueva.

La contrase�a que se deber� guardar estar� encryptada.

# Ejercicio 2

En la opci�n de **mantenimiento de empleados** se requiere poder subir las fotos de los empleados y poder visualizarlo en la pantalla de **mantenimiento de empleados**.
